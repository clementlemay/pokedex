export const environment = {
  production: true,
  pokeapiUrl: 'https://graphql-pokemon.now.sh',
  pokeapiUrlList: 'https://pokeapi.co/api/v2/pokemon/'
};
