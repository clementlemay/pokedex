import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from '../models/pokemon.models';

@Component({
  selector: 'app-caracteristics',
  templateUrl: './caracteristics.component.html',
  styleUrls: ['./caracteristics.component.css']
})
export class CaracteristicsComponent implements OnInit {
  @Input()
  pokemon: Pokemon;
  isCollapse = true;
  constructor() { }

  ngOnInit() {
  }

  Collapse() {
    this.isCollapse = !this.isCollapse;
    return this.isCollapse;

  }

}
