import { Component, OnDestroy } from '@angular/core';
import { TitleService } from '../services/title.service';
import { Observable, Subject } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnDestroy {
  private unsubscriber$: Subject<void> = new Subject<void>();

  title: string;
  searchControl = new FormControl('', Validators.required);
  constructor(titleService: TitleService) {
    const subscription = titleService.title$
      .pipe(takeUntil(this.unsubscriber$))
      .subscribe(value => this.title = value);
  }

  ngOnDestroy(): void {
    this.unsubscriber$.next();
    this.unsubscriber$.complete();
  }
}
