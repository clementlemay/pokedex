import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { PokemonService } from '../services/service';
import { Pokemon } from '../models/pokemon.models';
import { switchMap, map, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { Evolution } from '../models/evolution.model';
import { TitleService } from '../services/title.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit, OnDestroy {
  loading = true;
  pokemonDetails: Pokemon;
  evolutions: Evolution[];
  unsubscriber$: Subject<void> = new Subject<void>();


  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonService,
    private titleService: TitleService
  ) { }

  ngOnInit() {
    this.route.paramMap
      .pipe(
        map((paramMap: ParamMap): string => {
          return paramMap.get('name');
        }),
        switchMap((name: string): Observable<Pokemon> => {
          this.titleService.title = name;
          return this.pokemonService.getPokemon(name);
        }),
        takeUntil(this.unsubscriber$)
      )
      .subscribe(
        (pokemon: Pokemon) => {
          this.pokemonDetails = pokemon;
          this.evolutions = pokemon.evolutions;
          this.loading = false;
        }
      );
  }

  ngOnDestroy() {
    this.unsubscriber$.next();
    this.unsubscriber$.complete();
  }
}
