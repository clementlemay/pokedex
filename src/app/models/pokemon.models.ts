import { AttacksType } from './attacksType.model';
import { Evolution } from './evolution.model';

export interface Pokemon {
  name: string;
  types: string[];
  number: number;
  classification: string;
  weight: {
    minimum: string;
    maximum: string;
  };
  height: {
    minimum: string;
    maximum: string;
  };
  attacks: AttacksType;
  evolutions: Evolution[];
  evolutionRequirements: {
    amount: number;
    name: string;
  };
  image: string;
  resistant: [];
}
