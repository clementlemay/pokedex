import { Attack } from './attack.model';

export class AttacksType {
  fast: Attack[];
  special: Attack[];
}
