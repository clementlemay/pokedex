export class Attack {
  name: string;
  type: string;
  damage: number;
}
