import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonService } from '../services/service';
import { Pokemon } from '../models/pokemon.models';
import { first } from 'rxjs/operators';
import { TitleService } from '../services/title.service';
import { interval } from 'rxjs';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  pokemons: Pokemon[] = [];
  loading = true;
  constructor(
    private router: Router,
    private pokemonService: PokemonService,
    private titleService: TitleService) { }

  ngOnInit() {

    this.titleService.title = 'Pokemon';
    this.pokemonService.getPokemons()
      .pipe(
        first()
      )
      .subscribe(
        (pokemons: Pokemon[]) => {
          this.pokemons = pokemons;
          this.loading = false;
        }
      );
  }

  more(nomPokemon: string) {
    this.router.navigate([nomPokemon]);
  }

  scrollTop() {
    const subscription = interval(16)
      .subscribe(() => {
        const pos = window.pageYOffset;
        if (pos > 0) {
          window.scrollTo(0, pos - pos / 18); // how far to scroll on each step
        } else {
          subscription.unsubscribe();
        }
      });
  }
}
