import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TitleService {
  private _title$: Subject<string> = new Subject<string>();
  constructor() { }

  get title$(): Observable<string> {
    return this._title$.asObservable();
  }

  set title(value: string) {
    this._title$.next(value);
  }
}
