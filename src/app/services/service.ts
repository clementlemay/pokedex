import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pokemon } from '../models/pokemon.models';
import { pluck } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  constructor(private http: HttpClient) { }

  getPokemon(name: string): Observable<Pokemon> {
    const query = `{
      pokemon(name: "${name}") {
        name
        number
        classification
        types
        weight {
          minimum
          maximum
        }
        height {
          minimum
          maximum
        }
        attacks {
          fast {
            name
            type
            damage
          }
          special {
            name
            type
            damage
          }
        }
        evolutions {
          name
        }
        evolutionRequirements {
          amount
          name
        }
        image
        resistant
        weaknesses
      }
    }`;

    return this.http.get<{ data: { pokemon: Pokemon } }>(environment.pokeapiUrl + '?query=' + query)
      .pipe(
        pluck('data', 'pokemon')
      );
  }

  public getPokemons(): Observable<Pokemon[]> {
    const query = `{
      pokemons(first: 300) {
        name
        number
        types
        image
      }
    }`;

    return this.http.get<{ data: { pokemons: Pokemon[] } }>(environment.pokeapiUrl + '?query=' + query)
      .pipe(
        pluck('data', 'pokemons')
      );
  }
}
