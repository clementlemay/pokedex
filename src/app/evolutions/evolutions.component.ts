import { Component, OnInit, Input } from '@angular/core';
import { Evolution } from '../models/evolution.model';

@Component({
  selector: 'app-evolutions',
  templateUrl: './evolutions.component.html',
  styleUrls: ['./evolutions.component.css']
})
export class EvolutionsComponent implements OnInit {
  @Input() pokemonName: string;
  @Input() evolutions: Evolution[];
  constructor() { }

  ngOnInit() {
  }

}
