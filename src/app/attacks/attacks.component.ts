import { Component, OnInit, Input } from '@angular/core';
import { AttacksType } from '../models/attacksType.model';
import { DetailComponent } from '../detail/detail.component';
import { Observable } from 'rxjs';
import { Attack } from '../models/attack.model';

@Component({
  selector: 'app-attacks',
  templateUrl: './attacks.component.html',
  styleUrls: ['./attacks.component.css']
})

export class AttacksComponent {

  @Input()
  attacks: AttacksType;

  constructor() { }

}
